import { useSelector, useDispatch } from 'react-redux';
import { actions } from './index';
function Notes() {
  let data = useSelector((state) => state.todos);
  let dispatch = useDispatch();
  const deleteList = (e) => {
    let listId = Number(e.target.parentNode.id);
    dispatch(actions.deleteList(listId));
  };
  let editList = (e) => {
    let text = e.target.parentNode.firstChild.nextSibling;
    text.setAttribute('contenteditable', 'true');
    text.setAttribute('class', 'editTodo');
    e.target.style.display = 'none';
    e.target.previousSibling.style.display = 'none';
    e.target.nextSibling.style.display = 'inline';
  };
  let setValue = (e) => {
    if (e.keyCode === 13) {
      let node = e.target.parentNode.firstChild.nextSibling;
      node.setAttribute('class', 'endtodo');
      let listId = Number(e.target.parentNode.id);
      let text = e.target.innerText;
      let arr = [listId, text];
      dispatch(actions.editList(arr));
      e.target.setAttribute('contenteditable', 'false');
      console.log(e.target);
    }
  };
  const workDone = (e) => {
    let listId = Number(e.target.parentNode.id);
    dispatch(actions.workDone(listId));
  };
  const saveEditContent = (e) => {
    let listId = Number(e.target.parentNode.id);
    let node = e.target.parentNode.firstChild.nextSibling;
    let text = node.innerText;
    node.setAttribute('contenteditable', 'false');
    node.setAttribute('class', '');
    let arr = [listId, text];
    dispatch(actions.editList(arr));
    e.target.style.display = 'none';
    e.target.previousSibling.style.display = 'inline';
    e.target.previousSibling.previousSibling.style.display = 'inline';
  };

  return (
    <div>
      {data.map((ele) => {
        return (
          <li
            id={ele.id}
            className="todoList"
            style={{ textDecoration: ele.flag ? 'line-through' : 'none' }}
            key={ele.id}
          >
            <input
              type="checkbox"
              className="checkBox"
              onChange={workDone}
              checked={ele.flag}
            />
            <span onKeyDown={setValue}>{ele.name}</span>
            <button className="deleteButton" onClick={deleteList}>
              Delete
            </button>
            <button className="editButton " onClick={editList}>
              Edit
            </button>
            <button
              className="editButton "
              style={{ display: 'none', background: 'blue' }}
              onClick={saveEditContent}
            >
              Save
            </button>
          </li>
        );
      })}
    </div>
  );
}

export default Notes;
