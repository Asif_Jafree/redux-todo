import { configureStore, createSlice } from '@reduxjs/toolkit';
let arr = [];
if (localStorage.length === 0) {
  localStorage.setItem('todos', '[]');
} else {
  arr = [...JSON.parse(localStorage['todos'])];
}

const todoListSlice = createSlice({
  name: 'todos',
  initialState: { todos: [...arr], flag: 0 },
  reducers: {
    addlist(state, actions) {
      arr.push(actions.payload);
      localStorage.setItem('todos', JSON.stringify(arr));
      state.todos = [...arr];
    },
    deleteList(state, actions) {
      let index = arr.findIndex((ele) => ele.id === actions.payload);
      arr.splice(index, 1);
      localStorage.setItem('todos', JSON.stringify(arr));
      state.todos = [...arr];
    },
    editList(state, actions) {
      
      let index = arr.findIndex((ele) => ele.id === actions.payload[0]);
      let obj = {
        name: actions.payload[1],
        id: actions.payload[0],
        flag: false,
      };
      arr[index] = obj;
      localStorage.setItem('todos', JSON.stringify(arr));
      state.todos = [...arr];
     
      
    },
    workDone(state, actions) {
      let index = arr.findIndex((ele) => ele.id === actions.payload);

      let obj = {};

      if (arr[index].flag) {
        obj = { name: arr[index].name, id: arr[index].id, flag: false };
      } else {
        obj = { name: arr[index].name, id: arr[index].id, flag: true };
      }
      arr[index] = obj;
      localStorage.setItem('todos', JSON.stringify(arr));
      state.todos = [...arr];
    },
  },
});
export const actions = todoListSlice.actions;

const store = configureStore({
  reducer: todoListSlice.reducer,
});
export default store;
