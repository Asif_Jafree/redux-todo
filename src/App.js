import { useSelector, useDispatch } from 'react-redux';
import { actions } from './store';
import Notes from './store/Notes';

function App() {
  useSelector((state) => state.todos);
  const dispatch = useDispatch();
  const addlist = () => {
    let text = document.getElementById('text');
    let value = text.value;
    if (value.length < 2) return;
    let id = Math.floor(Math.random() * 10000000 + 100);
    let todoObj = { name: value, id: id, flag: false };
    dispatch(actions.addlist(todoObj));
    text.value = '';
  };
  return (
    <>
      <div className="container">
        <h1>Todo</h1>
        <div className="input-section">
          <input type="text" id="text" />
          <button className="addbtn" onClick={addlist}>
            Add
          </button>
        </div>
        <ul id="list" className="list">
          <Notes />
        </ul>
      </div>
    </>
  );
}

export default App;
